<?php
class Groups_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Get all purchase info about each product registered on the system
     * such as Buyer, purchase datetime and etc.
     */
    public function getDatatablesList($limit = null, $offset = 0)
    {
        // Col names by alias, used to order by colName, not alias, because
        // doesn't work when this is a datetime column
        $orderable = [
            'treated_datetime' => 'G.created_at'
        ];

        $query = $this->db
            ->select('SQL_CALC_FOUND_ROWS G.id, 
                        G.id, 
                        G.name,
                        T.name AS team_name,
                        DATE_FORMAT(G.created_at, \'%d/%m/%Y %H:%i\') as treated_datetime,
                        (SELECT GROUP_CONCAT(tmp.name  SEPARATOR ", ") FROM (SELECT u.name, UG.group_id FROM user_has_groups UG INNER JOIN users U ON UG.user_id=U.id ORDER BY u.name ASC) AS tmp WHERE tmp.group_id=G.id) AS users'
                , false)
            ->join('teams AS T', 'G.team_id = T.id', 'inner')
            ->from('groups AS G');

        //Ao filtrar por "todos" no datatables, ele envia -1
        if ( $limit > 0 ) {
            $query
                ->limit($limit)
                ->offset($offset);
        }

        $this->datatablesQuery($query, [], $orderable);
        $result = $query->get()->result();
        $foundRows = $this->db->select('FOUND_ROWS() as found_rows')->get()->result_array()[0]['found_rows'];

        return ['foundRows' => $foundRows, 'data' => $result];
    }

}